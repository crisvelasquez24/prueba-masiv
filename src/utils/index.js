import helper from "./helper";
import VueSidebarMenu from "./vue-sidebar-menu";
import fontAwesome from "./font-awesome";
import lodash from "./lodash";
import prime from "./prime";
import vueTippy from "./vue-tippy";
import can from "./permissions";

export default (app) => {
  app.use(helper);
  app.use(VueSidebarMenu);
  app.use(fontAwesome);
  app.use(lodash);
  app.use(prime);
  app.use(vueTippy);
  app.use(can);
};
