import { createRouter, createWebHistory } from "vue-router";
import defaultRoutes from "../views/home/router";
import auth from "../views/auth/router";
import checkAuth from "../middleware/checkAuth.middleware";
import inicio from "../views/inicio/router";
import errorsRoutes from "../views/errors/router";

const routes = [
  ...inicio,
  ...defaultRoutes,
  ...auth,
  ...errorsRoutes,
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 };
  },
  routes,
});

router.beforeEach(checkAuth);
router.beforeEach((to) => {
  const { title } = to.meta;
  document.title = `${title || "INICIO"} `;
});

export default router;
