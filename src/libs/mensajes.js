import Swal from 'sweetalert2'

const success = (text, title = 'Mensaje') => {
  Swal.fire({
    icon: 'success',
    title,
    text
  })
}
const messageSuccess = (text, title = 'Mensaje') => {
  Swal.fire({
    icon: 'success',
    title,
    text
  })
}

const error = (text, title = 'Error') => {
  Swal.fire({
    icon: 'error',
    title,
    text,
    backdrop: `
      rgba(0,0,123,0.4)
      url("/images/200.webp")
      left top
      no-repeat
    `
  })
}

const errorHtml = (title = 'Error', html) => {
  Swal.fire({
    icon: 'error',
    title,
    html
  })
}

const info = (text, title = 'Mensaje') => {
  Swal.fire({
    icon: 'info',
    title,
    text
  })
}

const warning = (text, title = 'Advertencia') => {
  Swal.fire({
    icon: 'warning',
    title,
    text
  })
}
const messageWarning = (text, title = 'Advertencia') => {
  Swal.fire({
    icon: 'warning',
    title,
    text
  })
}

const messageWarningDelete = (text = '', title = 'Estas seguro?', icon = 'question', html) => {
  return Swal.fire({
    icon,
    title,
    text,
    html,
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Eliminar!',
    cancelButtonText: 'No, cancelar!'
  })
}

const confirm = (text = '', title = 'Estas seguro?', icon = 'question', html) => {
  return Swal.fire({
    icon,
    title,
    text,
    html,
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'No'
  })
}
const messageConfirm = (text = '', title = 'Estas seguro?', icon = 'question', html) => {
  return Swal.fire({
    icon,
    title,
    text,
    html,
    showCancelButton: true,
    confirmButtonText: 'Confirmar',
    cancelButtonText: 'No'
  })
}
const messageConfirmSave = (text = '', title = 'Estas seguro?', icon = 'question', html) => {
  return Swal.fire({
    icon,
    title,
    text,
    html,
    showCancelButton: true,
    confirmButtonColor: '#24963E',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, Guardar!',
    cancelButtonText: 'No, cancelar!'
  })
}

export {
  success,
  confirm,
  error,
  errorHtml,
  info,
  warning,
  messageWarning,
  messageWarningDelete,
  messageSuccess,
  messageConfirm,
  messageConfirmSave
}
