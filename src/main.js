import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";
import "./libs";

import utils from "./utils";
import "./assets/css/app.css";
import "viewerjs/dist/viewer.css";
import VueViewer from "v-viewer";

import modalConfirm from "./components/modalConfirm/Main.vue";

const app = createApp(App);

app.component("modalConfirm", modalConfirm);

app.use(createPinia());
app.use(router);
app.use(VueViewer);
utils(app);

app.mount("#app");
