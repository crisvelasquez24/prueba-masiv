import { useAuthStore } from "@/stores/auth";
import { isProxy, toRaw } from "vue";
import getMenuService from "./services/getCategorias";
// Setup side menu
const findActiveMenu = (subMenu, route) => {
  let match = false;
  subMenu.forEach((item) => {
    if (item.pageName === route.name && !item.ignore) {
      match = true;
    } else if (!match && item.subMenu) {
      match = findActiveMenu(item.subMenu, route);
    }
  });
  return match;
};

const nestedMenu = (menu, route) => {
  // const auth = useAuthStore();
  // const user = auth.user;
  let newMenu = [];
  menu.forEach((item, key) => {
    item.title = item.name;
    if (typeof item !== "string") {
      let menuItem = menu[key];
      menuItem.active =
        (item.pageName === route.name ||
          (item.subMenu && findActiveMenu(item.subMenu, route))) &&
        !item.ignore;

      // if (item.subMenu) {
      //   menuItem.activeDropdown = findActiveMenu(item.subMenu, route);
      //   menuItem = {
      //     ...item,
      //     ...nestedMenu(item.subMenu, route),
      //   };
      // }
      // if (user.permissions.includes(item.permission) || !item.permission) {
      // if (item.child) {
      //   item.child = item.child.filter((e) =>
      //     user.permissions.includes(e.permission)
      //   );
      //   for (const i in item.child) {
      //     if (item.child[i].child) {
      //       item.child[i].child = item.child[i].child.filter((e) =>
      //         user.permissions.includes(e.permission)
      //       );
      //     }
      //   }
      // }
        newMenu.push(item);
      // }
    }
  });
  return newMenu;
  
};

const handleMenu = async () => {
  // console.log("handleMenu");
  await getMenuService()
    .then(({ data }) => {
      // console.log("get menu", data);
      return data;
    })
    // .catch((err) => {
    //   console.error(err.message);
    // });
};

const linkTo = (menu, router, event) => {
  if (menu.subMenu) {
    menu.activeDropdown = !menu.activeDropdown;
  } else {
    event.preventDefault();
    router.push({
      name: menu.pageName,
    });
  }
};

export { nestedMenu, linkTo };
