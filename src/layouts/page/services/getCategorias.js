import axios from "../../../libs/axios.lib";
export default (queryParams) =>
  axios.get(``, { params: { ...queryParams } });
