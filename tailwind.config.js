module.exports = {
  important: true,
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        "gradient-1": "#13c8dc",
        "gradient-2": "#2be2ec",
        "daily-blue": "#147BA8",
        "daily-green": "#1D9F6F",
        "daily-gris-1": "#B9B3C2",
        "daily-yellow-1": "#FAA600",
        "daily-yellow-2": "#f1ea0a",
      },
    },
    plugins: [],
  },
};
